
#include <SPI.h>
#include <Wire.h>


#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include <TimeLib.h>
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>

#include <TwitterWebAPI.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

// #include <ESP8266WebServer.h>
#include <ESPAsyncWebServer.h>
#include <FS.h>
//#include <AsyncTCP.h>
//#include <ESPAsyncWebServer.h>



#include <EEPROM.h>
#include <ArduinoJson.h>
#include "web_html.h"


#include <DHT.h>
#include <math.h>

// Web Socket para sincronizar con la web


// Deja una línea de código activa dependiendo de la versión de DHT que uses
#define DHTTYPE DHT11
const int DHTPin = 14;  //Comunicación de datos en el pin 0 (GPIO 16 -- D0)
// Iniciando sensor
DHT dht(DHTPin, DHTTYPE);

#define TWI_TIMEOUT 2000  // in msec
const char *ntp_server = "pool.ntp.org";  // time1.google.com, time.nist.gov, pool.ntp.org
int timezone = -5;                        // US Eastern timezone -05:00 HRS

// ALERTS PINOUT
// D6 - MAX TEMP
// D7 - MIN TEMP
// D8 - MAX HUM
// D3 - MIN HUM



String ssid = "";
String password = "";
String min_hum = "";
String max_hum = "";
String min_temp = "";
String max_temp = "";

String min_hum_real = "";
String max_hum_real = "";
String min_temp_real = "";
String max_temp_real = "";
const int SIZE_PASSWORD = 30;
const int SIZE_SSID = 30;

String SSID_FIXED_SIZE = "";
String PASSWORD_FIXED_SIZE = "";

// Web Server
AsyncWebServer server(80);
// ESP8266WebServer server(80);


WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, ntp_server, timezone*3600, 60000);  // NTP server pool, offset (in seconds), update interval (in milliseconds)


// Definir constantes
#define ANCHO_PANTALLA 128 // ancho pantalla OLED
#define ALTO_PANTALLA 64 // alto pantalla OLED
#define BEARER_TOKEN "A"

Adafruit_SSD1306 display(ANCHO_PANTALLA, ALTO_PANTALLA, &Wire, -1);

long int last_sample = millis();
long int last_publish = millis();
int BUTTON_PUBLISH = 0;

// Variables temporales
float celsiusTemp;
float celsiusTemp_prev = 0.0;
float fahrenheitTemp;
float humidityTemp;
float heat;
String incomingData = "";

const int MAX_WO_PUBLISH = 100000;
const int MIN_T_PUBLISH = 10000;

const long int DURATION_UNTIL_STAGE2 = 270000000;
const long int DURATION_UNTIL_STAGE3 = DURATION_UNTIL_STAGE2 + 180000000;
const long int DURATION_UNTIL_STAGE4 = DURATION_UNTIL_STAGE3 + 180000000;
bool no_more_twits = false;
bool try_con_wifi = true;


void setup() {

   Serial.begin(9600);
   pinMode(BUTTON_PUBLISH, INPUT_PULLUP) ;
   pinMode(LED_BUILTIN, OUTPUT);
   pinMode(D6, OUTPUT);
   pinMode(D7, OUTPUT);
   pinMode(D8, OUTPUT);
   pinMode(D3, OUTPUT);

   delay(100);

   dht.begin();

   if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
        Serial.println("No se encuentra la pantalla OLED");
        while (true);
    }

    loadDataEeprom();
    //display_message(password);
    //delay(5000);
    wifi_connect();
    first_screen();

    // Server Web
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
      handleRoot(request);
     });
    server.onNotFound([](AsyncWebServerRequest *request) {
      request->send(400, "text/plain", "Not found");
    });

    server.on("/sync", HTTP_GET, [](AsyncWebServerRequest *request){

      request->send(200, "text/html", write_json_output());
    });

    server.on("/set_max_temp", HTTP_GET, [](AsyncWebServerRequest *request){
      String value;
      if (request->hasParam("temp_max")) {
          value = request->getParam("temp_max")->value();
          max_temp_real = String(value);
          saveDataEepromFromRequest();
        }

      // handleRoot(request);
      request->redirect("/");
    });
    server.on("/set_min_temp", HTTP_GET, [](AsyncWebServerRequest *request){

      String value;
      if (request->hasParam("temp_min")) {
          value = request->getParam("temp_min")->value();
          min_temp_real = String(value);
          saveDataEepromFromRequest();
        }

      // handleRoot(request);
      request->redirect("/");
    });

    server.on("/set_max_hum", HTTP_GET, [](AsyncWebServerRequest *request){

      String value;
      if (request->hasParam("hum_max")) {
          value = request->getParam("hum_max")->value();
          max_hum_real = String(value);
          saveDataEepromFromRequest();
        }

      // handleRoot(request);
      request->redirect("/");
    });
    server.on("/set_min_hum", HTTP_GET, [](AsyncWebServerRequest *request){

      String value;
      if (request->hasParam("hum_min")) {
          value = request->getParam("hum_min")->value();
          min_hum_real = String(value);
          saveDataEepromFromRequest();
        }

      // handleRoot(request);
      request->redirect("/");
    });

    server.begin();
}

time_t prevDisplay = 0;

void loop() {

    //server.handleClient();



    while (Serial.available() > 0) {
        // read the incoming byte:
        incomingData = Serial.readStringUntil('\n');

        //DynamicJsonBuffer jsonBuffer;
        StaticJsonBuffer<1000> jsonBuffer;

        JsonObject& root = jsonBuffer.parseObject(incomingData);
        if (!root.success()) {
          display_message("parseObject() failed");
          delay(3000);
          break;
        }

        String output;
        root.printTo(output);
        display_message(output);
        delay(5000);

        const char* ssid_ = root["ssid"];
        ssid = String(ssid_).c_str();

        const char* password_ = root["key_wifi"];
        password = String(password_).c_str();

        const char* min_hum_ = root["min_hum"];
        min_hum = String(min_hum_).c_str();

        const char* max_hum_ = root["max_hum"];
        max_hum = String(max_hum_).c_str();

        const char* min_temp_ = root["min_temp"];
        min_temp = String(min_temp_).c_str();

        const char* max_temp_ = root["max_temp"];
        max_temp = String(max_temp_).c_str();

        max_temp_real = max_temp;
        min_temp_real = min_temp;
        max_hum_real = max_hum;
        min_hum_real = min_hum;

        fill_with_spaces();
        saveDataEeprom();

        display_message("Reconectando");
        try_con_wifi = true;
        wifi_connect();
        delay(2000);

    }

    if (millis() - last_sample > 2000)
    {


        celsiusTemp = dht.readTemperature();
        fahrenheitTemp = dht.readTemperature(true);
        humidityTemp = dht.readHumidity();
        heat = dht.computeHeatIndex(fahrenheitTemp, humidityTemp);

        send_uart_json();


        if (((celsiusTemp != celsiusTemp_prev) || (celsiusTemp_prev==0.0)) && !isnan(celsiusTemp) ){
          first_screen();
          show_temperature(celsiusTemp);
          celsiusTemp_prev = celsiusTemp;
        }
        set_pins_alerts();
        last_sample = millis();

    }

    bool state_button = digitalRead(BUTTON_PUBLISH);
    //Serial.print("Button: ");
    //Serial.print(state_button);


}



String fill_with_spaces()
{

    PASSWORD_FIXED_SIZE = password;
    SSID_FIXED_SIZE = ssid;
    for (int i=password.length(); i<SIZE_PASSWORD; i++)
    {
        PASSWORD_FIXED_SIZE += " ";
    }
    for (int i=ssid.length(); i<SIZE_SSID; i++)
    {
        SSID_FIXED_SIZE += " ";
    }
    for (int i=ssid.length(); i<SIZE_SSID; i++)
    {
        max_temp += " ";
    }
    for (int i=ssid.length(); i<SIZE_SSID; i++)
    {
        max_hum += " ";
    }
    for (int i=ssid.length(); i<SIZE_SSID; i++)
    {
        min_hum += " ";
    }
    for (int i=ssid.length(); i<SIZE_SSID; i++)
    {
        max_hum += " ";
    }

}

void send_uart_json()
{
      Serial.print("{'temperatureC':");
      Serial.print(celsiusTemp);
      Serial.print(",'humidity':");
      Serial.print(humidityTemp);
      Serial.print(",'heatC':");
      Serial.print(heat);
      Serial.print(",'wifi_connected':");
      Serial.print(WiFi.status() == WL_CONNECTED);
      Serial.print(",'ssid':'");
      Serial.print(ssid);

      Serial.print("','key_wifi':'");
      Serial.print(password);

      Serial.print("','min_hum':'");
      Serial.print(min_hum_real);
      Serial.print("','max_hum':'");
      Serial.print(max_hum_real);
      Serial.print("','min_temp':'");
      Serial.print(min_temp_real);
      Serial.print("','max_temp':'");
      Serial.print(max_temp_real);
      Serial.print("','temp_too_high':'");
      Serial.print(celsiusTemp >= max_temp_real.toInt());
      Serial.print("','temp_too_low':'");
      Serial.print(celsiusTemp <= min_temp_real.toInt());
      Serial.print("','hum_too_high':'");
      Serial.print(humidityTemp >= max_hum_real.toInt());
      Serial.print("','hum_too_low':'");
      Serial.print(humidityTemp <= min_hum_real.toInt());
      Serial.print("','local_ip':'");
      Serial.print(WiFi.localIP());

      Serial.println("'}");


}

String write_json_output()
{
      String output = "";
      output += "{\"temperatureC\":";
      output += celsiusTemp;
      output += (",\"humidity\":");
      output += (humidityTemp);
      output += (",\"heatC\":");
      output += (heat);
      output += (",\"wifi_connected\":");
      output += (WiFi.status() == WL_CONNECTED);
      output += ",\"ssid\":\"";
      output += (ssid);

      output += ("\",\"key_wifi\":\"");
      output += (password);

      output += "\",\"min_hum\":\"";
      output += (min_hum_real);
      output += ("\",\"max_hum\":\"");
      output += (max_hum_real);
      output += ("\",\"min_temp\":\"");
      output += (min_temp_real);
      output += ("\",\"max_temp\":\"");
      output += (max_temp_real);
      output += ("\",\"temp_too_high\":\"");
      output += (celsiusTemp >= max_temp_real.toInt());
      output += ("\",\"temp_too_low\":\"");
      output += (celsiusTemp <= min_temp_real.toInt());
      output += ("\",\"hum_too_high\":\"");
      output += (humidityTemp >= max_hum_real.toInt());
      output += ("\",\"hum_too_low\":\"");
      output += (humidityTemp <= min_hum_real.toInt());
      output += "\",\"local_ip\":\"";
      output += WiFi.localIP().toString();

      output += ("\"}");
      return output;


}



void wifi_connect()
{

    SSID_FIXED_SIZE.trim();
    ssid = SSID_FIXED_SIZE;

    PASSWORD_FIXED_SIZE.trim();
    password = PASSWORD_FIXED_SIZE;
    display_message("Reconectando to \n");
    display.println(ssid);
    display.println(password);
    display.display();
    delay(2000);
    WiFi.disconnect();
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    Serial.print("Conectando a:\t");
    Serial.println(ssid);
    long int waiting_connection = now();
    while ((WiFi.status() != WL_CONNECTED) && (try_con_wifi == true))
    {
        if (now() - waiting_connection < 10)
        {
            delay(200);
            Serial.print('.');
            display.print('.');
            // Enviar a pantalla
            display.display();
        }
        else{
            Serial.print('La password no es correcta');
            display_message("Imposible conectar con el wifi");
            try_con_wifi = false;
        }


    }
    if (WiFi.status() == WL_CONNECTED)
    {
        display_message("Conectado a ");
        display.println(ssid);
        display.display();
        Serial.println();
        Serial.print("Conectado a:\t");
        Serial.println(WiFi.SSID());
        Serial.print("IP address:\t");
        Serial.println(WiFi.localIP());
        display.println();
        display.print("Conectado a:\t");
        display.println(WiFi.SSID());
        display.print("IP address:\t");
        display.println(WiFi.localIP());
    }
    else
    {
        display_message("Password o SSID \n invalido");
        WiFi.disconnect();
    }

    delay(1000);

}

void first_screen(){
    // Limpiar buffer
    display.clearDisplay();

    // Tamaño del texto
    display.setTextSize(1);
    // Color del texto
    display.setTextColor(SSD1306_WHITE);
    // Posición del texto
    display.setCursor(0, 0);
    // Escribir texto
    display.println("LaCasaDelBicho");

    // Enviar a pantalla
    display.display();
}

void show_temperature(float temperature){
    first_screen();
    display.setTextSize(3);
    // Posición del texto
    display.setCursor(0, 20);
    display.setTextSize(1);
    display.println("Current Temperature:");
    display.setCursor(0, 40);
    display.setTextSize(3);
    display.print(temperature);
    display.println((char)167);

    // Enviar a pantalla
    display.display();


}

void display_message(String message){
    first_screen();
    display.setTextSize(3);
    // Posición del texto
    display.setCursor(0, 20);
    display.setTextSize(1);
    display.println(message);
    // Enviar a pantalla
    display.display();

}

void show_publishing_twitt(){
    first_screen();
    display.setCursor(0, 20);
    display.setTextSize(2);
    display.println("Publishing...");
    // Enviar a pantalla
    display.display();

}


/** Load WLAN credentials from EEPROM */
void loadDataEeprom() {
    EEPROM.begin(512);

    ////// Load SSID
    char data[SIZE_SSID+1]; //Max 100 Bytes
    int len=0;
    unsigned char k;
    k=EEPROM.read(0);
    while(k != '\0' && len<SIZE_SSID)   //Read until null character
    {
      k=EEPROM.read(len);
      data[len]=k;
      len++;
    }
    data[len]='\0';
    SSID_FIXED_SIZE = data;

    ////// Load PASSWORD
    char data2[SIZE_PASSWORD+1];
    len=0;
    k=EEPROM.read(SIZE_SSID);
    while(k != '\0' && len<SIZE_PASSWORD)   //Read until null character
    {
      k=EEPROM.read(SIZE_SSID+len);
      data2[len]=k;
      len++;
    }
    data2[len]='\0';
    PASSWORD_FIXED_SIZE = data2;

    ////// Load Min Hum
    char data_hum_min[5];
    len=0;
    k=EEPROM.read(SIZE_SSID+SIZE_PASSWORD+1);
    while(k != '\0' && len<5)
    {
      k=EEPROM.read(SIZE_SSID+SIZE_PASSWORD+len+1);
      data_hum_min[len]=k;
      len++;
    }
    data_hum_min[len]='\0';
    min_hum_real = data_hum_min;
    //min_hum.trim();

    ////// Load Max Hum
    char data_hum_max[5];
    len=0;
    k=EEPROM.read(SIZE_SSID+SIZE_PASSWORD+6);
    while(k != '\0' && len<5)
    {
      k=EEPROM.read(SIZE_SSID+SIZE_PASSWORD+6+len);
      data_hum_max[len]=k;
      len++;
    }
    data_hum_max[len]='\0';
    max_hum_real = data_hum_max;

    ////// Load Min Temp
    char data_temp_min[5];
    len=0;
    k=EEPROM.read(SIZE_SSID+SIZE_PASSWORD+11);
    while(k != '\0' && len<5)
    {
      k=EEPROM.read(SIZE_SSID+SIZE_PASSWORD+11+len);
      data_temp_min[len]=k;
      len++;
    }
    data_temp_min[len]='\0';
    min_temp_real = data_temp_min;

    ////// Load Max Temp
    char data_temp_max[5];
    len=0;
    k=EEPROM.read(SIZE_SSID+SIZE_PASSWORD+16);
    while(k != '\0' && len<5)
    {
      k=EEPROM.read(SIZE_SSID+SIZE_PASSWORD+16+len);
      data_temp_max[len]=k;
      len++;
    }
    data_temp_max[len]='\0';
    max_temp_real = data_temp_max;

    EEPROM.end();

    //fill_with_spaces();

}

/** Store WLAN credentials to EEPROM */
void saveDataEeprom() {
    EEPROM.begin(512);
    display_message("Saving SSID:");
    display.println(ssid);
    display.display();
    delay(5000);

    // Save SSID
    int _size = SSID_FIXED_SIZE.length();
    int i;
    for(i=0;i<_size;i++)
    {
      EEPROM.write(i,SSID_FIXED_SIZE[i]);
    }
    EEPROM.write(i+1,'\0');
    EEPROM.commit();

    // Save PASSWORD
    _size = PASSWORD_FIXED_SIZE.length();
    for(i=0;i<_size;i++)
    {
      EEPROM.write(SIZE_SSID+i,PASSWORD_FIXED_SIZE[i]);
    }
    EEPROM.write(SIZE_SSID+i+1,'\0');
    EEPROM.commit();


    // Save Min Hum
    _size = 5;
    for(i=0;i<_size;i++)
    {
      EEPROM.write(SIZE_SSID+SIZE_PASSWORD+i+1,min_hum_real[i]);
    }
    EEPROM.write(SIZE_SSID+SIZE_PASSWORD+i+1,'\0');
    EEPROM.commit();


    // Save Max Hum
    _size = 5;
    for(i=0;i<_size;i++)
    {
      EEPROM.write(SIZE_SSID+SIZE_PASSWORD+5+1+i,max_hum_real[i]);
    }
    EEPROM.write(SIZE_SSID+SIZE_PASSWORD+5+i+1,'\0');
    EEPROM.commit();

    // Save Min Temp
    _size = 5;
    for(i=0;i<_size;i++)
    {
      EEPROM.write(SIZE_SSID+SIZE_PASSWORD+5+5+1+i,min_temp_real[i]);
    }
    EEPROM.write(SIZE_SSID+SIZE_PASSWORD+5+5+i+1,'\0');
    EEPROM.commit();

    // Save Max Temp
    _size = 5;
    for(i=0;i<_size;i++)
    {
      EEPROM.write(SIZE_SSID+SIZE_PASSWORD+5+5+5+1+i,max_temp_real[i]);
    }
    EEPROM.write(SIZE_SSID+SIZE_PASSWORD+5+5+5+i+1,'\0');
    EEPROM.commit();
    EEPROM.end();

}


void saveDataEepromFromRequest() {
    EEPROM.begin(512);

    // Save SSID
    int _size = SSID_FIXED_SIZE.length();
    int i;
    for(i=0;i<_size;i++)
    {
      EEPROM.write(i,SSID_FIXED_SIZE[i]);
    }
    EEPROM.write(i+1,'\0');
    EEPROM.commit();

    // Save PASSWORD
    _size = PASSWORD_FIXED_SIZE.length();
    for(i=0;i<_size;i++)
    {
      EEPROM.write(SIZE_SSID+i,PASSWORD_FIXED_SIZE[i]);
    }
    EEPROM.write(SIZE_SSID+i+1,'\0');
    EEPROM.commit();


    // Save Min Hum
    _size = 5;
    for(i=0;i<_size;i++)
    {
      EEPROM.write(SIZE_SSID+SIZE_PASSWORD+i+1,min_hum_real[i]);
    }
    EEPROM.write(SIZE_SSID+SIZE_PASSWORD+i+1,'\0');
    EEPROM.commit();


    // Save Max Hum
    _size = 5;
    for(i=0;i<_size;i++)
    {
      EEPROM.write(SIZE_SSID+SIZE_PASSWORD+5+1+i,max_hum_real[i]);
    }
    EEPROM.write(SIZE_SSID+SIZE_PASSWORD+5+i+1,'\0');
    EEPROM.commit();

    // Save Min Temp
    _size = 5;
    for(i=0;i<_size;i++)
    {
      EEPROM.write(SIZE_SSID+SIZE_PASSWORD+5+5+1+i,min_temp_real[i]);
    }
    EEPROM.write(SIZE_SSID+SIZE_PASSWORD+5+5+i+1,'\0');
    EEPROM.commit();

    // Save Max Temp
    _size = 5;
    for(i=0;i<_size;i++)
    {
      EEPROM.write(SIZE_SSID+SIZE_PASSWORD+5+5+5+1+i,max_temp_real[i]);
    }
    EEPROM.write(SIZE_SSID+SIZE_PASSWORD+5+5+5+i+1,'\0');
    EEPROM.commit();
    EEPROM.end();

}

//password
//min_hum_real
//max_hum_real
//min_temp_real
//max_temp_real
//celsiusTemp >= max_temp_real.toInt()
//celsiusTemp <= min_temp_real.toInt()
//humidityTemp >= max_hum_real.toInt()
//humidityTemp <= min_hum_real.toInt()
//WiFi.localIP()
void set_pins_alerts(){

  if (celsiusTemp >= max_temp_real.toInt()){
    digitalWrite(D6, LOW);
  }else{
    digitalWrite(D6, HIGH);
  }
  if (celsiusTemp <= min_temp_real.toInt()){
    digitalWrite(D7, LOW);
  }else{
    digitalWrite(D7, HIGH);
  }
  if (humidityTemp >= max_hum_real.toInt()){
    digitalWrite(D8, LOW);
  }else{
    digitalWrite(D8, HIGH);
  }
  if (humidityTemp <= min_hum_real.toInt()){
    digitalWrite(D3, LOW);
  }else{
    digitalWrite(D3, HIGH);
  }
}


// HTML SERVER
void handleRoot(AsyncWebServerRequest *request) {
  String indicator_temp_min = "<button type='button' class='btn btn-secondary' style='margin-right:1em;' disabled id='button_temp_too_low'>Temp Min</button>";
  String indicator_temp_max = "<button type='button' class='btn btn-secondary' style='margin-right:1em;' disabled id='button_temp_too_high'>Temp Max</button>";
  String indicator_hum_min = "<button type='button' class='btn btn-secondary' style='margin-right:1em;' disabled id='button_hum_too_low'>Hum Min</button>";
  String indicator_hum_max = "<button type='button' class='btn btn-secondary' style='margin-right:1em;' disabled id='button_hum_too_high'>Hum Max</button>";

  String alert_temp = "green";
  if (celsiusTemp >= max_temp_real.toInt()){
    alert_temp = "<span style='font:red;'><b id='label_state_temp'>Cuidado!</b> <span  id='label_state_temp_2'>Demasiado Alta</span></span>";
    indicator_temp_max = "<button type='button' class='btn btn-danger' style='margin-right:1em;' disabled id='button_temp_too_high'>Temp Max</button>";
  }else{
    if(celsiusTemp <= min_temp_real.toInt()){
      alert_temp = "<span style='font:blue;'><b id='label_state_temp'>Cuidado!</b> <span  id='label_state_temp_2'>Demasiado Baja</span></span>";
      indicator_temp_min = "<button type='button' class='btn btn-danger' style='margin-right:1em;' disabled id='button_temp_too_low'>Temp Min</button>";
    }else{
      alert_temp = "<span style='font:red;'><b id='label_state_temp'>Tranqui!</b> <span  id='label_state_temp_2'>Todo Bien</span></span>";
    }
  }

  String alert_hum = "green";
  if (humidityTemp >= max_hum_real.toInt()){
    alert_hum = "<span style='font:red;'><b id='label_state_hum'>Cuidado!</b> <span  id='label_state_hum_2'>Demasiado Alta</span></span>";
    indicator_hum_max = "<button type='button' class='btn btn-danger' style='margin-right:1em;' disabled id='button_hum_too_high'>Hum Max</button>";
  }else{
    if(humidityTemp <= min_hum_real.toInt()){
      alert_hum = "<span style='font:blue;'><b id='label_state_hum'>Cuidado!</b><span  id='label_state_hum_2'> Demasiado Baja</span></span>";
      indicator_hum_min = "<button type='button' class='btn btn-danger' style='margin-right:1em;' disabled id='button_hum_too_low'>Hum Min</button>";
    }else{
      alert_hum = "<span style='font:red;'><b id='label_state_hum'>Tranqui!</b> <span  id='label_state_hum_2'>Todo Bien</span></span>";
    }
  }



  request->send(200, "text/html", "<html><link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css' integrity='sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z' crossorigin='anonymous'><script src='https://code.jquery.com/jquery-3.5.1.js' integrity='sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc='crossorigin='anonymous'></script><body style='background:#ad90ff;font-family: \"Courier New\", monospace;font-weight: bold;'><div id='particles-js' style='position:absolute;width:100%;'></div><center><h1><b><u>MiTerrario Web</u></b></h1></center><div class='row'><div class = 'col-md-4 col-sm-12' style='border-style:solid;border-width:1px;margin-top:2em;margin-left:2em;margin-right:5em;width: max-content;padding: 2em;background:#e8e8e8c4;'><h2>Temperatura : <b id='rt_temp'>" + String(celsiusTemp) + " C</b></h2><h2>Humedad: <b id='rt_humidity'>" + String(humidityTemp) + "%</b></h2><h2><u>Alarmas</u></h2><h3>Temperatura : </h3>"+alert_temp+"<h3>Humedad : </h3>"+alert_hum+"<br><h2>Salidas de Rele</h2>"+indicator_temp_min+indicator_temp_max+indicator_hum_min+indicator_hum_max+"</div><div class='col-md-7 col-sm-12' style='border-style: solid;border-width: 1px;margin-top:2em;margin-botton:2em;width: max-content;padding: 2em;background:whitesmoke;'><form action='/set_max_temp' method='get'><div class='form-group'><label for='formControlRange'>Temperatura Maxima</label><span id='amount_max_temp'> "+ String(max_temp_real) +"</span><input type='range' class='form-control-range'  name='temp_max' id='temp_max' value='"+ String(max_temp_real) +"' min=0 max=100><button class='btn btn-success'>Enviar</button></div></form><form action='/set_min_temp' method='get'><div class='form-group'><label for='formControlRange'>Temperatura Minima</label><span id='amount_min_temp'> "+ String(min_temp_real) +"</span><input type='range' class='form-control-range' name='temp_min' id='temp_min' value='"+ String(min_temp_real) +"' min=0 max=100><button class='btn btn-success'>Enviar</button></div></form><form action='/set_max_hum' method='get'><div class='form-group'><label for='formControlRange'>Humedad Maxima</label><span id='amount_max_hum'> "+ String(max_hum_real) +"</span><input type='range' class='form-control-range' name='hum_max' id='hum_max'  value='"+ String(max_hum_real) +"' min=0 max=100><button class='btn btn-success'>Enviar</button></div></form><form action='/set_min_hum' method='get'><div class='form-group'><label for='formControlRange'>Humedad Minima</label><span id='amount_min_hum'> "+ String(min_hum_real) +"</span><input type='range' class='form-control-range' name='hum_min' id='hum_min' value='"+ String(min_hum_real) +"' min=0 max=100><button class='btn btn-success'>Enviar</button></div></form></div></div></body><script src='https://cdn.jsdelivr.net/npm/particles.js@2.0.0/particles.js'></script><script src='https://cdnjs.cloudflare.com/ajax/libs/web-socket-js/1.0.0/web_socket.min.js' integrity='sha512-jtr9/t8rtBf1Sv832XjG1kAtUECQCqFnTAJWccL8CSC82VGzkPPih8rjtOfiiRKgqLXpLA1H/uQ/nq2bkHGWTQ==' crossorigin='anonymous'></script><script>$(document).ready(function(){particlesJS('particles-js');$('#temp_max').change(function(){var temp_max=$('#temp_max').val();$('#amount_max_temp').html(' ('+temp_max+' C)')});$('#temp_min').change(function(){var temp_min=$('#temp_min').val();$('#amount_min_temp').html(' ('+temp_min+' C)')});$('#hum_min').change(function(){var hum_min=$('#hum_min').val();$('#amount_min_hum').html(' ('+hum_min+' %)') });$('#hum_max').change(function(){var hum_max=$('#hum_max').val();$('#amount_max_hum').html(' ('+hum_max+' %)')});particlesJS('particles-js', {'particles':{'number':{'value':380,'density':{'enable':true,'value_area':800}},'color':{'value':'#ffffff'},'shape':{'type':'circle','stroke':{'width':0,'color':'#000000'},'polygon':{'nb_sides':5},'image':{'src':'img/github.svg','width':100,'height':100}},'opacity':{'value':0.5,'random':false,'anim':{'enable':false,'speed':1,'opacity_min':0.1,'sync':false}},'size':{'value':3,'random':true,'anim':{'enable':false,'speed':40,'size_min':0.1,'sync':false}},'line_linked':{'enable':true,'distance':150,'color':'#ffffff','opacity':0.4,'width':1},'move':{'enable':true,'speed':6,'direction':'none','random':false,'straight':false,'out_mode':'out','bounce':false,'attract':{'enable':false,'rotateX':600,'rotateY':1200}}},'interactivity':{'detect_on':'canvas','events':{'onhover':{'enable':true,'mode':'grab'},'onclick':{'enable':true,'mode':'push'},'resize':true},'modes':{'grab':{'distance':140,'line_linked':{'opacity':1}},}},'retina_detect':true});setInterval(function (){$.get('/sync', function(data){  var parse_data  = JSON.parse(data); $('#rt_temp').html(parse_data['temperatureC'] + ' C');  $('#rt_humidity').html(parse_data['humidity'] + ' %');  if (parse_data['temp_too_high']=='1'){    $('#button_temp_too_high').removeClass('btn-default').addClass('btn-danger');$('#label_state_temp').html('Cuidado!');$('#label_state_temp_2').html('Demasiado alta'); }else{    $('#button_temp_too_high').removeClass('btn-danger').addClass('btn-default'); $('#label_state_temp').html('Tranqui!');$('#label_state_temp_2').html('Todo Bien');} if (parse_data['temp_too_low'] == '1'){   $('#button_temp_too_low').removeClass('btn-default').addClass('btn-danger'); $('#label_state_temp').html('Cuidado!');$('#label_state_temp_2').html('Demasiado alta'); }else{    $('#button_temp_too_low').removeClass('btn-danger').addClass('btn-default'); $('#label_state_temp').html('Tranqui!');$('#label_state_temp_2').html('Todo Bien'); } if (parse_data['hum_too_high'] == '1'){   $('#button_hum_too_high').removeClass('btn-default').addClass('btn-danger'); $('#label_state_hum').html('Cuidado!');$('#label_state_hum_2').html('Demasiado alta'); }else{    $('#button_hum_too_high').removeClass('btn-danger').addClass('btn-default'); $('#label_state_hum').html('Tranqui!');$('#label_state_hum_2').html('Todo Bien'); } if (parse_data['hum_too_low'] == '1'){    $('#button_hum_too_low').removeClass('btn-default').addClass('btn-danger'); $('#label_state_hum').html('Cuidado!');$('#label_state_hum_2').html('Demasiado alta');}else{    $('#button_hum_too_low').removeClass('btn-danger').addClass('btn-default');$('#label_state_hum').html('Tranqui!');$('#label_state_hum_2').html('Todo Bien'); }})}, 1000);})</script></html>");   // Send HTTP status 200 (Ok) and send some text to the browser/client
}
