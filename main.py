
import sys
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QApplication, QPushButton, QLineEdit, QDialogButtonBox
from PySide2.QtCore import QFile, QObject
from PySide2 import QtCore

import glob
import serial
import json
# from PyQt5.QtCore import QTimer,QDateTime


import time

class Ui(QObject):

    def __init__(self, ui_file, parent=None):
        super(Ui, self).__init__(parent)
        ui_file = QFile(ui_file)
        ui_file.open(QFile.ReadOnly)

        loader = QUiLoader()
        self.window = loader.load(ui_file)
        ui_file.close()
        self.sync_esp = False
        self.window.buttonBox.button(QDialogButtonBox.Cancel).setToolTip('Salir del programa')
        self.window.buttonBox.button(QDialogButtonBox.Ok).setToolTip('Aplicar configuracion')

        self.window.list_ports.addItems(self.serial_ports())
        self.window.list_ports.itemClicked.connect(self.check_port)

        self.window.buttonBox.accepted.connect(self.applicate_settings)
        self.window.buttonBox.rejected.connect(self.close_app)
        self.window.button_sync.clicked.connect(self.sync_values)



        self.serial_port = None
        self.threads = 0

        self.window.show()

    def sync_values(self):

        if not self.connected:
            print (self.connected)
            self.get_data_from_serial()

    def applicate_settings(self):

        print ("Aplicando configuracion")
        ssid = self.window.ssid_input.toPlainText()
        key_wifi = self.window.key_input.toPlainText()
        hum_min = self.window.spin_hum_min.value()
        hum_max = self.window.spin_hum_max.value()
        temp_max = self.window.spin_temp_max.value()
        temp_min = self.window.spin_temp_min.value()
        print ("Key: %s" % key_wifi)
        print ("SSID: %s" % ssid)
        print ("Hum Min: %s" % hum_min)
        print ("Hum Max: %s" % hum_max)
        print ("Temp Min: %s" % temp_min)
        print ("Temp Max: %s" % temp_max)
        if self.serial_port != None:
            json_to_esp = json.dumps({
                "key_wifi":key_wifi,
                "ssid":ssid,
                "min_hum": str(hum_min) if str(hum_min) != "" else "0",
                "max_hum": str(hum_max) if str(hum_max) != "" else "0",
                "min_temp": str(temp_min) if str(temp_min) != "" else "0",
                "max_temp": str(temp_max) if str(temp_max) != "" else "0",
            }).encode()
            print (json_to_esp)
            self.serial_port.write(json_to_esp)
        else:
            self.window.warnings.setText("El puerto no es valido")


    def close_app(self):

        sys.exit()

    def check_port(self, item):

        self.port_selected = item
        self.current_port = item.text()
        self.connected = False
        self.window.indicator_conesp8266.setStyleSheet('QLabel {background-color: orange; color: black;border-radius : 10px;}')
        # self.get_data_from_serial(item)
        if self.threads == 0:
            self.threads += 1
            self.timer_serial = QtCore.QTimer()
            self.timer_serial.setInterval(3)
            self.timer_serial.timeout.connect(self.get_data_from_serial)
            self.timer_serial.start()
            # thread = threading.Thread(target=self.get_data_from_serial, args=(item,), daemon=True)
            # self.threads.append(thread)
            # self.threads[len(self.threads) - 1].start()

    def get_data_from_serial(self):

        self.window.button_sync.setEnabled(False)
        item = self.port_selected
        self.connected = True
        self.serial_port = serial.Serial(item.text())
        # self.serial_port.flush()
        try:
            line = self.serial_port.readline()
            try:
                formated_line = str(line).replace("b\"", "").replace("\"", "").replace("\'", "\"").replace("\\r\\n", "")
                message = json.loads(formated_line)
                self.set_indicators(message)
                print (message)
            except Exception as ex:
                print (str(ex))
                pass

        except Exception as e:
            print (str(e))
            self.connected = False
        self.serial_port.close()

        return

    def set_indicators(self, message):

        if not self.sync_esp:
            if "min_hum" in message and message["min_hum"] != "":
                self.window.spin_hum_min.setValue(float(message["min_hum"]))
                self.sync_esp = True
            if "max_hum" in message and message["max_hum"] != "":
                self.window.spin_hum_max.setValue(float(message["max_hum"]))
                self.sync_esp = True
            if "min_temp" in message and message["min_temp"] != "":
                self.window.spin_temp_min.setValue(float(message["min_temp"]))
                self.sync_esp = True
            if "max_temp" in message and message["max_temp"] != "":
                self.window.spin_temp_max.setValue(float(message["max_temp"]))
                self.sync_esp = True


        if "ssid" in message and message["ssid"] != "":
            self.window.ssid_input.clear()
            self.window.ssid_input.insertPlainText(message["ssid"])

        if "key_wifi" in message and message["key_wifi"] != "":
            self.window.key_input.clear()
            self.window.key_input.insertPlainText(message["key_wifi"])

        if "temp_too_low" in message and message["temp_too_low"] != "":
            if message["temp_too_low"] == "1":
                self.window.indicator_temperature_alarm.setStyleSheet('QLabel {background-color: blue;}')

        if "temp_too_high" in message and message["temp_too_high"] != "":
            if message["temp_too_high"] == "1":
                self.window.indicator_temperature_alarm.setStyleSheet('QLabel {background-color: red;}')

        if "temp_too_high" in message and message["temp_too_high"] != "" and "temp_too_low" in message and message["temp_too_low"] != "":
            if message["temp_too_high"] != "1" and message["temp_too_low"] != "1":
                self.window.indicator_temperature_alarm.setStyleSheet('QLabel {background-color: green;}')

        if "hum_too_low" in message and message["hum_too_low"] != "":
            if message["hum_too_low"] == "1":
                self.window.indicator_hum_alarm.setStyleSheet('QLabel {background-color: blue;}')

        if "hum_too_high" in message and message["hum_too_high"] != "":
            if message["hum_too_high"] == "1":
                self.window.indicator_hum_alarm.setStyleSheet('QLabel {background-color: red;}')

        if "hum_too_high" in message and message["hum_too_high"] != "" and "hum_too_low" in message and message["hum_too_low"] != "":
            if message["hum_too_high"] != "1" and message["hum_too_low"] != "1":
                self.window.indicator_hum_alarm.setStyleSheet('QLabel {background-color: green;}')


        self.window.temperature_lcd.smallDecimalPoint()
        self.window.temperature_lcd.setSmallDecimalPoint (True)
        self.window.humidity_lcd.smallDecimalPoint()
        self.window.humidity_lcd.setSmallDecimalPoint (True)
        self.window.temperature_lcd.display(message["temperatureC"])
        self.window.humidity_lcd.display(message["humidity"])
        self.window.ip_value_label.setText("Take a look of your device on <b>http://%s</b>" % message["local_ip"])
        self.window.indicator_conesp8266.setStyleSheet('QLabel {background-color: green; color: black;border-radius : 10px;}')
        if message["wifi_connected"] == 1:
            self.window.indicator_wifi.setStyleSheet('QLabel {background-color: green; color: black;border-radius : 10px;}')
        else:
            self.window.indicator_wifi.setStyleSheet('QLabel {background-color: red; color: black;border-radius : 10px;}')

    def serial_ports(self):
        """ Lists serial port names

        """
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')

        result = []
        for port in ports:
            try:
                # s = serial.Serial(port)
                # s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass
        return result

    def get_items_json_coming(self):

        """
        Items json from esp3286
        """

        return ["humidity", "temperaureC", "heatC"]


if __name__ == '__main__':
    app = QApplication(sys.argv)
    # ui = Ui('ui/bicho_home.ui')
    ui = Ui('ui/bicho_home_sin_tweeter.ui')

    sys.exit(app.exec_())
